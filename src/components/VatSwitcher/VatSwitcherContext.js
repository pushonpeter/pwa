import {createContext} from 'react';

export const VatSwitcherContext = createContext(true);
