import React, {useContext} from 'react';
import {shape, string} from 'prop-types';
import {VatSwitcherContext} from './VatSwitcherContext';
import Cookies from 'universal-cookie';
import ToggleSwitch from "../Inputs";
import defaultClasses from './vatSwitcher.css';
import {mergeClasses} from '@magento/venia-ui/lib/classify';

const cookies = new Cookies();

const VatSwitcher = props => {
    const classes = mergeClasses(defaultClasses, props.classes);

    const vat = useContext(VatSwitcherContext);

    function vatSwitchHandler() {
        const newVatStatus = !vat.vatStatus;
        vat.setVatStatus(newVatStatus);
        cookies.set('vatStatus', newVatStatus)
    }

    return (

        <div className={classes.vatSwitch}>
            <ToggleSwitch
                htmlId="vatSwitch"
                labelVal={vat.vatStatus ? "VAT Incl." : "VAT Excl."}
                handler={vatSwitchHandler}
                isDefaultChecked={vat.vatStatus}
            />
        </div>
    )
}

VatSwitcher.propTypes = {
    classes: shape({
        vatSwitch: string
    })
}

export default VatSwitcher;
