import React from 'react';
import {shape, string} from 'prop-types';
import defaultClasses from './test.css';
import {mergeClasses} from '@magento/venia-ui/lib/classify';

const Test = props => {
    const classes = mergeClasses(defaultClasses, props.classes);

    return (
        <div className={classes.messageContainer}>
            <p>{props.message}</p>
        </div>
    );
}

Test.propTypes = {
    classes: shape({
        messageContainer: string
    }),

    message: string.isRequired
}

export default Test;
