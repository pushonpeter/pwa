import React from 'react';
import {bool, func, shape, string} from 'prop-types';
import defaultClasses from './toggleSwitch.css';
import {mergeClasses} from '@magento/venia-ui/lib/classify';

const ToggleSwitch = props => {
    const classes = mergeClasses(defaultClasses, props.classes);

    return (
        <label htmlFor={props.htmlId} className={classes.switch}>
            <span className={classes.visuallyHidden}>{props.labelVal}</span>
            <input type="checkbox"
                   id={props.htmlId}
                   className={classes.checkbox}
                   onChange={props.handler}
                   data-label={props.labelVal}
                   defaultChecked={props.isDefaultChecked}/>
            <span className={`${classes.slider} ${classes.round}`}></span>
        </label>
    );
}

ToggleSwitch.propTypes = {
    classes: shape({
        switch: string,
        checkbox: string,
        slider: string,
        round: string,
        visuallyHidden: string
    }),
    htmlId: string.isRequired,
    labelVal: string.isRequired,
    handler: func.isRequired,
    isDefaultChecked: bool.isRequired
}

export default ToggleSwitch;
