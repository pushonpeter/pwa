import React, {useContext} from 'react';
import {shape, string} from 'prop-types';

import {useMiniCart} from '@magento/peregrine/lib/talons/MiniCart/useMiniCart';

import Body from '@magento/venia-ui/lib/components/MiniCart/body';
import Footer from '@magento/venia-ui/lib/components/MiniCart/footer';
import Header from '@magento/venia-ui/lib/components/MiniCart/header';
import Mask from '@magento/venia-ui/lib/components/MiniCart/mask';
import defaultClasses from '@magento/venia-ui/lib/components/MiniCart/miniCart.css';
import {mergeClasses} from '@magento/venia-ui/lib/classify';
import {VatSwitcherContext} from "../../../../components/VatSwitcher/VatSwitcherContext";

const MiniCart = props => {
    const {
        cartItems,
        cartState,
        currencyCode,
        handleBeginEditItem,
        handleDismiss,
        handleEndEditItem,
        handleClose,
        isEditingItem,
        isLoading,
        isMiniCartMaskOpen,
        isOpen,
        isUpdatingItem,
        numItems,
        setStep,
        shouldShowFooter,
        step,
        subtotal
    } = useMiniCart();

    const vat = useContext(VatSwitcherContext);

    const footer = shouldShowFooter ? (
        <Footer
            currencyCode={currencyCode}
            isMiniCartMaskOpen={isMiniCartMaskOpen}
            numItems={numItems}
            setStep={setStep}
            step={step}
            subtotal={subtotal}
        />
    ) : null;

    const classes = mergeClasses(defaultClasses, props.classes);
    const rootClass = isOpen ? classes.root_open : classes.root;


    return (
        <aside className={rootClass}>
            <Header closeDrawer={handleClose} isEditingItem={isEditingItem}/>
            {vat.vatStatus ? "True" : "False"}
            <Body
                beginEditItem={handleBeginEditItem}
                cartItems={cartItems}
                closeDrawer={handleClose}
                currencyCode={currencyCode}
                endEditItem={handleEndEditItem}
                isCartEmpty={cartState.isEmpty}
                isEditingItem={isEditingItem}
                isLoading={isLoading}
                isUpdatingItem={isUpdatingItem}
            />
            <Mask isActive={isMiniCartMaskOpen} dismiss={handleDismiss}/>
            {footer}
        </aside>
    );
};

MiniCart.propTypes = {
    classes: shape({
        header: string,
        root: string,
        root_open: string,
        title: string
    })
};

export default MiniCart;
