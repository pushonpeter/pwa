import React, {useCallback, useState} from 'react';
import {array, func, shape, string} from 'prop-types';

import {useToasts} from '@magento/peregrine';
import {useApp} from '@magento/peregrine/lib/talons/App/useApp';

import {HeadProvider, Title} from '@magento/venia-ui/lib/components/Head';
import Main from '@magento/venia-ui/lib/components/Main';
import Mask from '@magento/venia-ui/lib/components/Mask';
import MiniCart from '@magento/venia-ui/lib/components/MiniCart';
import Navigation from '@magento/venia-ui/lib/components/Navigation';
import Routes from '@magento/venia-ui/lib/components/Routes';
import ToastContainer from '@magento/venia-ui/lib/components/ToastContainer';
import Icon from '@magento/venia-ui/lib/components/Icon';
import {VatSwitcherContext} from '../../../../components/VatSwitcher/VatSwitcherContext'
import Cookies from "universal-cookie";
import {AlertCircle as AlertCircleIcon, CloudOff as CloudOffIcon, Wifi as WifiIcon} from 'react-feather';

const OnlineIcon = <Icon src={WifiIcon} attrs={{width: 18}}/>;
const OfflineIcon = <Icon src={CloudOffIcon} attrs={{width: 18}}/>;
const ErrorIcon = <Icon src={AlertCircleIcon} attrs={{width: 18}}/>;

const ERROR_MESSAGE = 'Sorry! An unexpected error occurred.';

const cookies = new Cookies();

const App = props => {
    const {markErrorHandled, renderError, unhandledErrors} = props;

    const [, {addToast}] = useToasts();

    let defaultVatStatus = true;

    if (cookies.get('vatStatus') === 'false') {
        defaultVatStatus = false;
    }

    const [vatStatus, setVatStatus] = useState(defaultVatStatus);


    const handleIsOffline = useCallback(() => {
        addToast({
            type: 'error',
            icon: OfflineIcon,
            message: 'You are offline. Some features may be unavailable.',
            timeout: 3000
        });
    }, [addToast]);

    const handleIsOnline = useCallback(() => {
        addToast({
            type: 'info',
            icon: OnlineIcon,
            message: 'You are online.',
            timeout: 3000
        });
    }, [addToast]);

    const handleError = useCallback(
        (error, id, loc, handleDismissError) => {
            const errorToastProps = {
                icon: ErrorIcon,
                message: `${ERROR_MESSAGE}\nDebug: ${id} ${loc}`,
                onDismiss: remove => {
                    handleDismissError();
                    remove();
                },
                timeout: 15000,
                type: 'error'
            };

            addToast(errorToastProps);
        },
        [addToast]
    );

    const talonProps = useApp({
        handleError,
        handleIsOffline,
        handleIsOnline,
        markErrorHandled,
        renderError,
        unhandledErrors
    });

    const {hasOverlay, handleCloseDrawer} = talonProps;

    if (renderError) {
        return (
            <HeadProvider>
                <Title>{`Home Page - ${STORE_NAME}`}</Title>
                <Main isMasked={true}/>
                <Mask isActive={true}/>
                <ToastContainer/>
            </HeadProvider>
        );
    }

    return (
        <HeadProvider>
            <VatSwitcherContext.Provider value={{vatStatus, setVatStatus}}>
                <Title>{`Home Page - ${STORE_NAME}`}</Title>
                <Main isMasked={hasOverlay}>
                    <Routes/>
                </Main>
                <Mask isActive={hasOverlay} dismiss={handleCloseDrawer}/>
                <Navigation/>
                <MiniCart/>
                <ToastContainer/>
            </VatSwitcherContext.Provider>
        </HeadProvider>
    );
};

App.propTypes = {
    markErrorHandled: func.isRequired,
    renderError: shape({
        stack: string
    }),
    unhandledErrors: array
};

export default App;
