import React from 'react';
import {number, shape, string} from 'prop-types';
import {Link, resourceUrl} from '@magento/venia-drivers';
import {Price} from '@magento/peregrine';
import {transparentPlaceholder} from '@magento/peregrine/lib/util/images';
import {UNCONSTRAINED_SIZE_KEY} from '@magento/peregrine/lib/talons/Image/useImage';
import RichContent from '@magento/venia-ui/lib/components/RichContent';
import {mergeClasses} from '@magento/venia-ui/lib/classify';
import Image from '@magento/venia-ui/lib/components/Image';
import defaultClasses from './item.css';

// The placeholder image is 4:5, so we should make sure to size our product
// images appropriately.
const IMAGE_WIDTH = 300;
const IMAGE_HEIGHT = 375;

// Gallery switches from two columns to three at 640px.
const IMAGE_WIDTHS = new Map()
    .set(640, IMAGE_WIDTH)
    .set(UNCONSTRAINED_SIZE_KEY, 840);

// TODO: get productUrlSuffix from graphql when it is ready
const productUrlSuffix = '.html';

const ItemPlaceholder = ({classes}) => (
    <div className={classes.root_pending}>
        <div className={classes.images_pending}>
            <Image
                alt="Placeholder for gallery item image"
                classes={{
                    image: classes.image_pending,
                    root: classes.imageContainer
                }}
                src={transparentPlaceholder}
            />
        </div>
        <div className={classes.name_pending}/>
        <div className={classes.price_pending}/>
    </div>
);


const GalleryItem = props => {
    const {item} = props;

    const classes = mergeClasses(defaultClasses, props.classes);

    if (!item) {
        return <ItemPlaceholder classes={classes}/>;
    }

    const {name, price, price_range, small_image, url_key, sku, description} = item;
    const productLink = resourceUrl(`/${url_key}${productUrlSuffix}`);
    const regularPriceValue = price.regularPrice.amount.value;
    const lowestPriceValue = price_range.minimum_price.final_price.value;

    let priceBlock = <Price
        value={regularPriceValue}
        currencyCode={price.regularPrice.amount.currency}
    />

    if (lowestPriceValue < regularPriceValue) {
        priceBlock =
            <Price value={lowestPriceValue}
                   currencyCode={price_range.minimum_price.final_price.currency}/>
    }

    return (
        <div className={classes.root}>
            <Link to={productLink} className={classes.images}>
                <Image
                    alt={name}
                    classes={{
                        image: classes.image,
                        root: classes.imageContainer
                    }}
                    height={IMAGE_HEIGHT}
                    resource={small_image}
                    widths={IMAGE_WIDTHS}
                />
            </Link>
            <div className={classes.productDetails}>
                <Link to={productLink} className={classes.name}>
                    <span>{name}</span>
                </Link>

                <RichContent html={description.html}/>

                <span className={classes.sku}>SKU: {sku}</span>

            </div>
            <div className={classes.priceContainer}>
                {lowestPriceValue < regularPriceValue ? <span className={classes.priceFromLabel}>From</span> : null}
                <div className={classes.priceValue}>
                    {priceBlock}
                </div>
            </div>
        </div>
    );
};

GalleryItem.propTypes = {
    classes: shape({
        image: string,
        imageContainer: string,
        imagePlaceholder: string,
        image_pending: string,
        images: string,
        images_pending: string,
        name: string,
        name_pending: string,
        productDescription: string,
        price: string,
        price_pending: string,
        priceLabel: string,
        priceContainer: string,
        priceValue: string,
        priceFromLabel: string,
        root: string,
        root_pending: string
    }),
    item: shape({
        id: number.isRequired,
        name: string.isRequired,
        sku: string.isRequired,
        small_image: string.isRequired,
        url_key: string.isRequired,
        price: shape({
            regularPrice: shape({
                amount: shape({
                    value: number.isRequired,
                    currency: string.isRequired
                }).isRequired
            }).isRequired
        }).isRequired,
        price_range: shape({
            minimum_price: shape({
                final_price: shape({
                    value: number.isRequired,
                    currency: string.isRequired
                }).isRequired
            }).isRequired
        }).isRequired
    })
};

export default GalleryItem;
